import {makeAutoObservable} from 'mobx';

export interface TestResult {
  testName: string;
  result: number;
}

export class ResultsStore {
  constructor() {
    makeAutoObservable(this);
  }

  results: TestResult[] = [];

  meanResults: {mean: number; testName: string}[] = [];

  addTestResult = (testName: string, result: number) => {
    this.results = [{testName, result}, ...this.results];
  };

  calculateMeans = () => {
    const newMeans: {
      [key: string]: {sum: number; counter: number; mean: number};
    } = {};
    const results = [...this.results];
    results.pop(); // Remove last element, which is the first webview test
    results.forEach(test => {
      if (!newMeans[test.testName]) {
        newMeans[test.testName] = {sum: test.result, counter: 1, mean: 0};
      } else {
        const prevValue = newMeans[test.testName];
        newMeans[test.testName] = {
          sum: prevValue.sum + test.result,
          counter: prevValue.counter + 1,
          mean: 0,
        };
      }
    });
    this.meanResults = Object.entries(newMeans).map(obj => {
      return {mean: Math.round(obj[1].sum / obj[1].counter), testName: obj[0]};
    });
    console.log(this.meanResults);
  };
}

export default new ResultsStore();
