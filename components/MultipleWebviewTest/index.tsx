import React, {useRef, useState} from 'react';
import {WebView} from 'react-native-webview';
import {WebViewMessageEvent} from 'react-native-webview';
import {WebViewSource} from 'react-native-webview/lib/WebViewTypes';
import resultsStore from '../../stores/resultsStore';

export interface MultiWebviewTestProps {
  source: WebViewSource;
  multiplier: number;
}

const injectedJavaScript = `
window.onload = (event) => {
  window.ReactNativeWebView.postMessage(document.body.clientHeight);
};`;

export default function WebviewTest({
  testName,
  source,
  multiplier,
}: MultiWebviewTestProps & {testName: string}) {
  const [height, setHeight] = useState(0);
  var [renderTimes] = useState<number[]>(Array(multiplier).fill(0));
  const ref = useRef<WebView>(null);
  const [t1] = useState(performance.now());

  return (
    <>
      {Array.from({length: multiplier}, (_, id) => (
        <WebView
          key={id}
          style={{height: height, overflow: 'hidden'}}
          injectedJavaScript={injectedJavaScript}
          originWhitelist={['*']}
          source={source}
          ref={id === multiplier - 1 ? ref : null}
          allowFileAccessFromFileURLs
          allowFileAccess
          //@ts-ignore
          webviewDebuggingEnabled
          onMessage={(e: WebViewMessageEvent) => {
            setHeight(Number(e.nativeEvent.data));
          }}
          onLoadEnd={() => {
            const t2 = performance.now();
            renderTimes[id] = t2 - t1;
            if (!renderTimes.includes(0)) {
              resultsStore.addTestResult(testName, Math.max(...renderTimes));
              ref.current?.injectJavaScript(`
              var message = document.body.scrollHeight;
              window.ReactNativeWebView.postMessage(message);
              message = undefined;
              `);
            }
          }}
        />
      ))}
    </>
  );
}
