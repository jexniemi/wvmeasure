import React, {Profiler} from 'react';
import {StyleProp, View} from 'react-native';
import resultsStore from '../../stores/resultsStore';

export interface NativeTestProps {
  multiplier?: number;
  children: React.ReactNode;
  containerStyle?: StyleProp<any>;
}

export default function NativeTest({
  testName,
  multiplier = 1,
  children,
  containerStyle,
}: NativeTestProps & {testName: string}) {
  return (
    <Profiler
      id={testName}
      onRender={(id, phase, actualDuration) => {
        console.log(actualDuration);
        resultsStore.addTestResult(testName, actualDuration);
      }}>
      <View style={containerStyle}>
        {Array.from({length: multiplier}, (_, id: number) => (
          <React.Fragment key={id}>{children}</React.Fragment>
        ))}
      </View>
    </Profiler>
  );
}
