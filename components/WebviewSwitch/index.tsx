import React, {useRef, useState} from 'react';
import {WebView} from 'react-native-webview';
import {WebViewSource} from 'react-native-webview/lib/WebViewTypes';
import resultsStore from '../../stores/resultsStore';
import {Platform} from 'react-native';

const sources: WebViewSource[] = [
  Platform.OS === 'ios'
    ? require('../../assets/empty.html')
    : {
        uri: 'file:///android_asset/empty.html',
      },
  Platform.OS === 'ios'
    ? require('../../assets/text10000.html')
    : {
        uri: 'file:///android_asset/text10000.html',
      },
  Platform.OS === 'ios'
    ? require('../../assets/text50000.html')
    : {
        uri: 'file:///android_asset/text50000.html',
      },
  Platform.OS === 'ios'
    ? require('../../assets/text100000.html')
    : {
        uri: 'file:///android_asset/text100000.html',
      },
  Platform.OS === 'ios'
    ? require('../../assets/10images.html')
    : {
        uri: 'file:///android_asset/10images.html',
      },
  Platform.OS === 'ios'
    ? require('../../assets/100images.html')
    : {
        uri: 'file:///android_asset/100images.html',
      },
  Platform.OS === 'ios'
    ? require('../../assets/500images.html')
    : {
        uri: 'file:///android_asset/500images.html',
      },
];

export default function WebviewSwitchTest({
  testName,
  calculateResults,
}: {
  testName: string;
  calculateResults: () => void;
}) {
  var [testNumber] = useState([0]);
  const [sourceIndex, setSourceIndex] = useState<number>(0);
  /* const [setHeight] = useState(1); */
  const ref = useRef<WebView>(null);

  // const runAutomaticTests = useCallback(() => {}, []);

  const t1 = performance.now();

  return (
    <>
      <WebView
        style={{height: 35944}} // Height of the largest view
        ref={ref}
        originWhitelist={['*']}
        source={sources[sourceIndex]}
        allowFileAccessFromFileURLs
        allowFileAccess
        //@ts-ignore
        webviewDebuggingEnabled
        /* onMessage={(e: WebViewMessageEvent) => {
          setHeight(Number(e.nativeEvent.data));
        }} */
        onLoadEnd={() => {
          var t2 = performance.now();
          resultsStore.addTestResult(testName + sourceIndex, t2 - t1);
          testNumber[0] = testNumber[0] + 1;
          if (testNumber[0] < sources.length * 10) {
            setTimeout(() => {
              let newSourceIndex =
                sourceIndex < sources.length - 1 ? sourceIndex + 1 : 0;
              setSourceIndex(newSourceIndex);
            }, 5000);
          } else {
            calculateResults();
          }
        }}
      />
    </>
  );
}
