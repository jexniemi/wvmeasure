import React, {useRef, useState} from 'react';
import {WebView} from 'react-native-webview';
import {WebViewMessageEvent} from 'react-native-webview';
import {WebViewSource} from 'react-native-webview/lib/WebViewTypes';
import resultsStore from '../../stores/resultsStore';

export interface WebviewTestProps {
  source: WebViewSource;
}

export default function WebviewTest({
  testName,
  source,
}: WebviewTestProps & {testName: string}) {
  const [height, setHeight] = useState(1);
  const ref = useRef<WebView>(null);
  const t1 = performance.now();

  return (
    <WebView
      style={{height}}
      ref={ref}
      originWhitelist={['*']}
      source={source}
      allowFileAccessFromFileURLs
      allowFileAccess
      //@ts-ignore
      webviewDebuggingEnabled
      onMessage={(e: WebViewMessageEvent) => {
        setHeight(Number(e.nativeEvent.data));
      }}
      onLoadEnd={() => {
        var t2 = performance.now();
        resultsStore.addTestResult(testName, t2 - t1);
        ref.current?.injectJavaScript(
          `
          var message = document.body.scrollHeight;
          window.ReactNativeWebView.postMessage(message);
          message = undefined;
          `,
        );
      }}
    />
  );
}
