import {observer} from 'mobx-react-lite';
import React, {useCallback, useState} from 'react';
import {
  Alert,
  Button,
  Clipboard,
  Platform,
  SafeAreaView,
  ScrollView,
  StatusBar,
  Text,
  View,
} from 'react-native';
import MultipleWebviewTest, {
  MultiWebviewTestProps,
} from './components/MultipleWebviewTest';
import NativeTest, {NativeTestProps} from './components/NativeTest';
import WebviewTest, {WebviewTestProps} from './components/WebviewTest';
import resultsStore from './stores/resultsStore';
import {
  emptyHtml,
  text100000html,
  text10000html,
  text50000html,
} from './testFiles/html';
import {useKeepAwake} from '@sayem314/react-native-keep-awake';
import {ImgTest, Text10000} from './testFiles/tests';
import {delay} from './helpers';
import WebviewSwitchTest from './components/WebviewSwitch';

export const tests: {
  [key: string]:
    | (NativeTestProps & {isNative: boolean})
    | WebviewTestProps
    | (MultiWebviewTestProps & {isMultiWebview: boolean});
} = {
  'webview empty': {source: {html: emptyHtml}},
  'native empty': {
    multiplier: 1,
    children: <View />,
    isNative: true,
  },
  'webview 10 000 char': {source: {html: text10000html}},
  'webview 50 000 char': {source: {html: text50000html}},
  'webview 100 000 char': {source: {html: text100000html}},
  'native 10 000 char': {
    children: <Text10000 />,
    isNative: true,
  },
  'native 50 000 char': {
    multiplier: 5,
    children: <Text10000 />,
    isNative: true,
  },
  'native 100 000 char': {
    multiplier: 10,
    children: <Text10000 />,
    isNative: true,
  },
  'webview 10 images': {
    source:
      Platform.OS === 'ios'
        ? require('./assets/10images.html')
        : {
            uri: 'file:///android_asset/10images.html',
          },
  },
  'webview 100 images': {
    source:
      Platform.OS === 'ios'
        ? require('./assets/100images.html')
        : {
            uri: 'file:///android_asset/100images.html',
          },
  },
  'webview 500 images': {
    source:
      Platform.OS === 'ios'
        ? require('./assets/500images.html')
        : {
            uri: 'file:///android_asset/500images.html',
          },
  },
  'native 10 images': {
    multiplier: 10,
    children: <ImgTest />,
    isNative: true,
  },
  'native 100 images': {
    multiplier: 100,
    children: <ImgTest />,
    isNative: true,
  },
  'native 500 images': {
    multiplier: 500,
    children: <ImgTest />,
    isNative: true,
  },
  'multiwebview 5': {
    multiplier: 5,
    isMultiWebview: true,
    source:
      Platform.OS === 'ios'
        ? require('./assets/imageAndText.html')
        : {
            uri: 'file:///android_asset/imageAndText.html',
          },
  },
  'multiwebview 10': {
    multiplier: 10,
    isMultiWebview: true,
    source:
      Platform.OS === 'ios'
        ? require('./assets/imageAndText.html')
        : {
            uri: 'file:///android_asset/imageAndText.html',
          },
  },
  'multiwebview 15': {
    multiplier: 15,
    isMultiWebview: true,
    source:
      Platform.OS === 'ios'
        ? require('./assets/imageAndText.html')
        : {
            uri: 'file:///android_asset/imageAndText.html',
          },
  },
  'multiwebview 20': {
    multiplier: 20,
    isMultiWebview: true,
    source:
      Platform.OS === 'ios'
        ? require('./assets/imageAndText.html')
        : {
            uri: 'file:///android_asset/imageAndText.html',
          },
  },
  'multiwebview 25': {
    multiplier: 25,
    isMultiWebview: true,
    source:
      Platform.OS === 'ios'
        ? require('./assets/imageAndText.html')
        : {
            uri: 'file:///android_asset/imageAndText.html',
          },
  },
};
export interface TestResult {
  testName: string;
  result: number;
}

function App(): JSX.Element {
  useKeepAwake();
  const [activeScreen, setActiveScreen] = useState<string>('');

  const calculateResults = useCallback(() => {
    resultsStore.calculateMeans();
    const results = JSON.stringify({
      tests: resultsStore.results,
      means: resultsStore.meanResults,
    });

    Alert.alert(
      'Tests finished',
      'Copy results',
      [
        {
          text: 'Copy message',
          onPress: () => Clipboard.setString(results),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }, []);

  const launchTest = useCallback(async () => {
    const testKeys = Object.keys(tests);

    setActiveScreen('');
    await delay(1000);
    setActiveScreen('webview empty');
    await delay(3000);
    setActiveScreen('');

    await delay(2000);
    for await (const testKey of testKeys) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      for await (const i of Array(10)) {
        setActiveScreen(testKey);
        await delay(3000);
        setActiveScreen('');
        await delay(2000);
      }
    }
    // calculateResults();
    setActiveScreen('webviewSwitch');
  }, []);

  const renderScreen = useCallback(() => {
    if (activeScreen === 'webviewSwitch') {
      return (
        <WebviewSwitchTest
          calculateResults={calculateResults}
          testName="webview switch"
        />
      );
    }
    if (tests[activeScreen]) {
      const props = tests[activeScreen];
      if (activeScreen.includes('native') && 'isNative' in props) {
        return (
          <NativeTest
            {...props}
            containerStyle={{flexDirection: 'row', flexWrap: 'wrap'}}
            testName={activeScreen}
          />
        );
      } else if (
        activeScreen.includes('multiwebview') &&
        'isMultiWebview' in props
      ) {
        return <MultipleWebviewTest {...props} testName={activeScreen} />;
      } else if (activeScreen.includes('webview') && !('isNative' in props)) {
        return <WebviewTest {...props} testName={activeScreen} />;
      }
    } else {
      return (
        <View>
          {Object.keys(tests).map(testName => (
            <Button
              title={testName}
              key={testName}
              onPress={() => setActiveScreen(testName)}
            />
          ))}
        </View>
      );
    }
  }, [activeScreen, calculateResults]);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <StatusBar />
      <View style={{marginTop: 2, backgroundColor: 'black', width: '100%'}}>
        <Button title="Home" onPress={() => setActiveScreen('')} />
      </View>
      <View style={{marginTop: 2, backgroundColor: 'red', width: '100%'}}>
        <Button
          title="WebView switch test"
          onPress={() => setActiveScreen('webviewSwitch')}
        />
      </View>
      <View style={{marginTop: 2, backgroundColor: 'violet', width: '100%'}}>
        <Button title="Calculate Results" onPress={calculateResults} />
      </View>
      {/* <View style={{backgroundColor: 'yellow', width: '100%'}}>
        <Button
          title="Webview switch test"
          onPress={() => setActiveScreen('webviewSwitch')}
        />
      </View> */}
      <View style={{marginTop: 2, backgroundColor: 'orange', width: '100%'}}>
        <Button title="Run tests" onPress={launchTest} />
      </View>
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={{flex: 1, margin: 10}}>
        {renderScreen()}
      </ScrollView>
      <TestResults />
    </SafeAreaView>
  );
}

export const TestResults = observer(() => {
  return (
    <View style={{backgroundColor: 'black'}}>
      <Text style={{color: 'white'}}>
        Test:
        {resultsStore.results.length > 0
          ? resultsStore.results[0].testName +
            ' - ' +
            resultsStore.results[0].result
          : ''}
        ms
      </Text>
    </View>
  );
});

export default App;
