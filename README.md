# WVMeasure

WVMeasure is an experimental React Native application for measuring rendering times of WebViews and React Native components.

# Instructions

You need Mac and Xcode to install this application on iOS device. Android Studio is required to install this application on an Android device.

1. Clone repository on local machine
2. In the project root run `$ npm install`
3. To run the project on iOS simulator execute the command `$ npm run ios`, to run the project on Android emulator use the command `$ npm run android`

## Install on Android device

Build and install .apk by running `npm run apk`, the apk will be generated in `./android/app/build/outputs/apk/release/app-release.apk`.

Install .apk to device by running `adb install './android/app/build/outputs/apk/release/app-release.apk'`

## Install on iOS device

To install release versio of app on iPhone:

- Open Xcode
- Product -> Scheme -> Edit scheme
- Choose build configuration -> release (revert this to debug if debug build is needed)
- Select emulator or USB connected device
- Click run

## Running tests

Make sure that on Android screen sleep timer is set to 30 minutes or however long is needed to run the tests. On iOS, device should stay awake automatically.

Install application on Android or iOS device or emulator using instructions above. After launching the application, press the button `RUN TESTS`. This will run all of the configured tests automatically, and after the tests are done, an alert dialogue is displayed. Do not press any of the buttons on the screen while tests are running. Pressing the `COPY RESULTS` button in the alert copies JSON string of the test results in your device's clipboard, from where it can be pasted to any on-device text editor or email application for further inspection.

To reset the tests and results, close the application completely (from background as well), and launch the application again. Make sure you do this if you want to re-run the tests.
## Configuring tests

Tests are configured in the App.tsx file in the root directory. Additional tests need to be manually configured. Follow examples in App.tsx to make either new WebView tests or React Native tests. Any number of tests can be configured. The purpose of WebviewSwitch component is to test content rendering with preloaded WebViews, instead of loading the full WebView component.

Place any HTML documents or images in /assets directory, and afterwards make sure you run `npm run copy` before launching the application. This copies all of the HTML files in relevant android directories so that they are available on devices when running the application. Additionally, there are no images required in HTML, the HTML files can be configured as strings in testFiles/html.tsx.

Adjust latencies between tests and number of tests runs from App.tsx delay() functions or WebviewSwitch.tsx for the tests with preloaded WebViews.
